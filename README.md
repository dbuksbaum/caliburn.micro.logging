Caliburn.Micro.Logging
======================
                       
Source code to the [Caliburn.Micro.Logging nuget package](http://nuget.org/List/Packages/Caliburn.Micro.Logging).
See [blog post](http://buksbaum.us/2011/07/04/introducing-caliburn-micro-logging/)
See [blog post](http://buksbaum.us/2011/07/06/introducing-caliburn-micro-logging-log4net/)
See [blog post](http://buksbaum.us/2011/07/06/introducing-caliburn-micro-logging-nlog/)

The Logging library is compiled with DEBUG attribute set, so ALL message sent to the Debug logger will be logged.

__Version 1.2.1 - 2011/08/12__
	* Upgraded .NET 4 and Silverlight 4 versions to Caliburn.Micro 1.2.0
	* Created a psake build script to be used in Continious Integration
	* Moved samples into their own solution

__Version 1.2 - 2011/07/05__
  * Added GlobalAssemblyInfo.cs and VersionAssemblyInfo.cs to contain common AssemblyInfo attributes
  * Removed DebugLogger.cs from the Caliburn.Micro.Logging assembly
  * Added DebugLogger.cs to the NuGet content directory
  * Support for .NET 4.0 and NET 4.0 Client Profile

Caliburn.Micro.Logging.log4net
==============================
__Version 1.2.1 - 2011/08/11__
	* Upgraded .NET 4 and Silverlight 4 versions to Caliburn.Micro 1.2.0
	* Created a psake build script to be used in Continious Integration

__Version 1.0 - 2011/07/06__
  * Support for .NET 4.0 only

Caliburn.Micro.Logging.NLog
===========================
__Version 1.2.1 - 2011/08/11__
	* Upgraded .NET 4 and Silverlight 4 versions to Caliburn.Micro 1.2.0
	* Created a psake build script to be used in Continious Integration
__Version 1.0 - 2011/07/06__
  * Support for .NET 4.0 and .NET 4.0 Client Profile
