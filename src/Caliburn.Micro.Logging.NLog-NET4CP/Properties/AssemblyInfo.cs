using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

#if !SILVERLIGHT
//[assembly: SuppressIldasmAttribute()]
[assembly: CLSCompliantAttribute(true )]
#endif
[assembly: ComVisibleAttribute(false)]
[assembly: AssemblyTitleAttribute("Caliburn.Micro.Logging.NLog-NET4CP 1.2.1.0")]
[assembly: AssemblyDescriptionAttribute("Logging for Caliburn.Micro")]
[assembly: AssemblyCompanyAttribute("David Buksbaum - http://buksbaum.us")]
[assembly: AssemblyProductAttribute("Caliburn.Micro.Logging 1.2.1.0")]
[assembly: AssemblyCopyrightAttribute("Copyright © David Buksbaum 2009-2011")]
[assembly: AssemblyVersionAttribute("1.2.1.0")]
[assembly: AssemblyInformationalVersionAttribute("1.2.1.0 / 0000000")]
[assembly: AssemblyFileVersionAttribute("1.0.0.4")]
[assembly: AssemblyDelaySignAttribute(false)]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyConfiguration("Release")]

